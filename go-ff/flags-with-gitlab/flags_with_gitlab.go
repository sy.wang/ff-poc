package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	ffclient "github.com/thomaspoignant/go-feature-flag"
	"github.com/thomaspoignant/go-feature-flag/ffcontext"
	"github.com/thomaspoignant/go-feature-flag/retriever/gitlabretriever"
	"log"
	"os"
	"time"
)

func main() {
	err := ffclient.Init(ffclient.Config{
		PollingInterval: 10 * time.Second,
		Logger:          log.New(os.Stdout, "", 0),
		Context:         context.Background(),
		Retriever: &gitlabretriever.Retriever{
			RepositorySlug: "sy.wang/ff-poc",
			Branch:         "main",
			FilePath:       "config/flags.gitlab.yaml",
			Timeout:        3 * time.Second,
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer ffclient.Close()

	user := ffcontext.NewEvaluationContext(uuid.NewString())
	userCanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", user, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if !userCanEnableMetrics {
		fmt.Println("user without admin access cannot enable metrics")
	}

	anonymousUser := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("anonymous", true).
		Build()
	anonymUserCanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", anonymousUser, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if !anonymUserCanEnableMetrics {
		fmt.Println("anonymous user without admin access cannot enable metrics")
	}

	adminUser := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("firstname", "Grace").
		AddCustom("email", "grace@gmail.com").
		AddCustom("admin", true).
		Build()
	CanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", adminUser, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if CanEnableMetrics {
		fmt.Println("Admin user (Grace) can enable metrics")
	}
}
