module gitlab.com/sy.wang/ff-poc/go-ff

go 1.21.3

require (
	github.com/google/uuid v1.6.0
	github.com/thomaspoignant/go-feature-flag v1.23.0
)

require (
	github.com/BurntSushi/toml v1.3.2 // indirect
	github.com/antlr4-go/antlr/v4 v4.13.0 // indirect
	github.com/apache/arrow/go/arrow v0.0.0-20200730104253-651201b0f516 // indirect
	github.com/apache/thrift v0.14.2 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/klauspost/compress v1.17.4 // indirect
	github.com/nikunjy/rules v1.5.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.18 // indirect
	github.com/xitongsys/parquet-go v1.6.2 // indirect
	github.com/xitongsys/parquet-go-source v0.0.0-20230830030807-0dd610dbff1d // indirect
	golang.org/x/exp v0.0.0-20240112132812-db7319d0e0e3 // indirect
	golang.org/x/xerrors v0.0.0-20231012003039-104605ab7028 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
