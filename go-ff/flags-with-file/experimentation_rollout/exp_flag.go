package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	ffclient "github.com/thomaspoignant/go-feature-flag"
	"github.com/thomaspoignant/go-feature-flag/ffcontext"
	"github.com/thomaspoignant/go-feature-flag/retriever/fileretriever"
)

const numUsers = 100

func main() {
	err := ffclient.Init(ffclient.Config{
		PollingInterval: 10 * time.Second,
		Logger:          log.New(os.Stdout, "", 0),
		Context:         context.Background(),
		Retriever: &fileretriever.Retriever{
			Path: "config/flags.rollout_exp.yaml",
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer ffclient.Close()

	for true {
		var users = make([]ffcontext.EvaluationContext, numUsers)
		for i := 0; i < numUsers; i++ {
			users[i] = ffcontext.NewEvaluationContext(uuid.NewString())
		}

		usrWithTrueVal := 0
		time.Sleep(1 * time.Second)
		for _, usr := range users {
			res, _ := ffclient.BoolVariation("experimentation-flag", usr, false)
			if res {
				usrWithTrueVal++
			}
		}
		fmt.Println("number of users with TRUE: ", usrWithTrueVal)
		fmt.Println("percentage of users with TRUE: ", float64(usrWithTrueVal)/numUsers)
	}

	/*

			Expected: 0.5 (50%) for users with TRUE out of 100 users (given that experimentation ran in the time range defined
			in the YAML file)
			Actual: range of percentage is 0.40 - 0.60, not cnosistent
				number of users with TRUE:  43
				percentage of users with TRUE:  0.43
				number of users with TRUE:  48
				percentage of users with TRUE:  0.48
				number of users with TRUE:  51
				percentage of users with TRUE:  0.51
				number of users with TRUE:  55
				percentage of users with TRUE:  0.55
				number of users with TRUE:  52
				percentage of users with TRUE:  0.52
				number of users with TRUE:  51
				percentage of users with TRUE:  0.51
				number of users with TRUE:  50
				percentage of users with TRUE:  0.5
				number of users with TRUE:  58
				percentage of users with TRUE:  0.58
				number of users with TRUE:  44
				percentage of users with TRUE:  0.44
				number of users with TRUE:  53
				percentage of users with TRUE:  0.53
				number of users with TRUE:  51
				percentage of users with TRUE:  0.51
				number of users with TRUE:  52
				percentage of users with TRUE:  0.52
				number of users with TRUE:  53
				percentage of users with TRUE:  0.53
				number of users with TRUE:  46
				percentage of users with TRUE:  0.46
				number of users with TRUE:  48
				percentage of users with TRUE:  0.48
				number of users with TRUE:  53
				percentage of users with TRUE:  0.53
				number of users with TRUE:  52
				percentage of users with TRUE:  0.52
				number of users with TRUE:  57
				percentage of users with TRUE:  0.57
				number of users with TRUE:  41
				percentage of users with TRUE:  0.41
				number of users with TRUE:  60
				percentage of users with TRUE:  0.6
				number of users with TRUE:  45
				percentage of users with TRUE:  0.45
				number of users with TRUE:  41
				percentage of users with TRUE:  0.41
				number of users with TRUE:  49
				percentage of users with TRUE:  0.49
				number of users with TRUE:  52
				percentage of users with TRUE:  0.52
				number of users with TRUE:  54
				percentage of users with TRUE:  0.54
				number of users with TRUE:  56
				percentage of users with TRUE:  0.56
				number of users with TRUE:  46
				percentage of users with TRUE:  0.46
				number of users with TRUE:  51
				percentage of users with TRUE:  0.51
				number of users with TRUE:  52
				percentage of users with TRUE:  0.52

			Expected: 0.5 (50%) for users with TRUE out of 1000 users (given that experimentation ran in the time range defined
		in the YAML file)
			Actual: range of percentage is 0.474 - 0.544, slight improvement when there are more users
				number of users with TRUE:  505
				percentage of users with TRUE:  0.505
				number of users with TRUE:  488
				percentage of users with TRUE:  0.488
				number of users with TRUE:  491
				percentage of users with TRUE:  0.491
				number of users with TRUE:  486
				percentage of users with TRUE:  0.486
				number of users with TRUE:  507
				percentage of users with TRUE:  0.507
				number of users with TRUE:  519
				percentage of users with TRUE:  0.519
				number of users with TRUE:  488
				percentage of users with TRUE:  0.488
				number of users with TRUE:  502
				percentage of users with TRUE:  0.502
				number of users with TRUE:  475
				percentage of users with TRUE:  0.475
				number of users with TRUE:  500
				percentage of users with TRUE:  0.5
				number of users with TRUE:  474
				percentage of users with TRUE:  0.474
				number of users with TRUE:  512
				percentage of users with TRUE:  0.512
				number of users with TRUE:  456
				percentage of users with TRUE:  0.456
				number of users with TRUE:  485
				percentage of users with TRUE:  0.485
				number of users with TRUE:  519
				percentage of users with TRUE:  0.519
				number of users with TRUE:  488
				percentage of users with TRUE:  0.488
				number of users with TRUE:  510
				percentage of users with TRUE:  0.51
				number of users with TRUE:  484
				percentage of users with TRUE:  0.484
				number of users with TRUE:  511
				percentage of users with TRUE:  0.511
				number of users with TRUE:  519
				percentage of users with TRUE:  0.519
				number of users with TRUE:  489
				percentage of users with TRUE:  0.489
				number of users with TRUE:  481
				percentage of users with TRUE:  0.481
				number of users with TRUE:  499
				percentage of users with TRUE:  0.499
				number of users with TRUE:  495
				percentage of users with TRUE:  0.495
				number of users with TRUE:  489
				percentage of users with TRUE:  0.489
				number of users with TRUE:  479
				percentage of users with TRUE:  0.479
				number of users with TRUE:  508
				percentage of users with TRUE:  0.508
				number of users with TRUE:  544
				percentage of users with TRUE:  0.544

			Q: every time this gets ran, the result is inconsistent, but should be 50/50 as long as it's in the range?
	*/

}
