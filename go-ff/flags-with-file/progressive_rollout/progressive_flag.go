package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/thomaspoignant/go-feature-flag/exporter/logsexporter"

	"log"
	"os"
	"time"

	ffclient "github.com/thomaspoignant/go-feature-flag"
	"github.com/thomaspoignant/go-feature-flag/ffcontext"
	"github.com/thomaspoignant/go-feature-flag/retriever/fileretriever"
)

func main() {
	err := ffclient.Init(ffclient.Config{
		PollingInterval: 10 * time.Second,
		Logger:          log.New(os.Stdout, "", 0),
		Context:         context.Background(),
		Retriever: &fileretriever.Retriever{
			Path: "config/flags.rollout_progressive.yaml",
		},
		DataExporter: ffclient.DataExporter{
			FlushInterval:    10,
			MaxEventInMemory: 2,
			Exporter: &logsexporter.Exporter{
				LogFormat: "[{{ .FormattedDate}}] user=\"{{ .UserKey}}\", flag=\"{{ .Key}}\", value=\"{{ .Value}}\", variation=\"{{ .Variation}}\"",
			},
		},
	})
	// Check init errors.
	if err != nil {
		log.Fatal(err)
	}
	// defer closing ffclient
	defer ffclient.Close()

	// create users
	user1 := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("beta", "true").
		Build()
	user2 := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("anonymous", "true").
		Build()
	user3 := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("omega", "true").
		Build()
	// Call multiple time the same flag to see the change in time.
	for true {
		time.Sleep(1 * time.Second)
		fmt.Println(ffclient.BoolVariation("progressive-flag", user1, false))
		fmt.Println()
		fmt.Println(ffclient.BoolVariation("progressive-flag", user2, false))
		fmt.Println()
		fmt.Println(ffclient.BoolVariation("progressive-flag", user3, false))
		fmt.Println("---------------------------------------------------------------------------")
	}
	/*
		    progressiveRollout:
		      initial:
		        variation: A
		        percentage: 0
		        date: 2024-02-16T11:21:56-08:00
		      end:
		        variation: B
		        percentage: 100
		        date: 2024-02-16T11:22:56-08:00


		[2024-02-16T11:21:53-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="true", variation="A"
		true <nil>

		[2024-02-16T11:21:54-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="true", variation="A"
		true <nil>

		[2024-02-16T11:21:54-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="true", variation="A"
		true <nil>
		---------------------------------------------------------------------------
		.......
		---------------------------------------------------------------------------
		[2024-02-16T11:22:02-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="false", variation="B"
		true <nil>

		[2024-02-16T11:22:03-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="true", variation="A"
		true <nil>

		[2024-02-16T11:22:03-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="true", variation="A"
		false <nil>
		---------------------------------------------------------------------------
		[2024-02-16T11:22:03-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="false", variation="B"
		true <nil>

		[2024-02-16T11:22:04-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="true", variation="A"
		true <nil>

		[2024-02-16T11:22:04-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="true", variation="A"
		false <nil>
		---------------------------------------------------------------------------
		.......
		---------------------------------------------------------------------------
		[2024-02-16T11:22:10-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="false", variation="B"
		false <nil>

		[2024-02-16T11:22:11-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="false", variation="B"
		true <nil>

		[2024-02-16T11:22:11-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="true", variation="A"
		false <nil>
		---------------------------------------------------------------------------
		.......
		---------------------------------------------------------------------------
		[2024-02-16T11:22:48-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="false", variation="B"
		false <nil>

		[2024-02-16T11:22:49-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="false", variation="B"
		true <nil>

		[2024-02-16T11:22:49-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="true", variation="A"
		false <nil>
		---------------------------------------------------------------------------
		[2024-02-16T11:22:49-08:00] user="a3dcb836-08c9-485e-ab1c-392aae524475", flag="progressive-flag", value="false", variation="B"
		false <nil>

		[2024-02-16T11:22:50-08:00] user="7bf9dd1d-9d23-4191-a63a-c4f1a8ee4010", flag="progressive-flag", value="false", variation="B"
		false <nil>

		[2024-02-16T11:22:50-08:00] user="e7fa55f5-18a7-4cb4-9adf-51ccebd0ad93", flag="progressive-flag", value="false", variation="B"

	*/

}
