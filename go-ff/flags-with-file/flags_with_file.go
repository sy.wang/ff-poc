package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/google/uuid"
	ffclient "github.com/thomaspoignant/go-feature-flag"
	"github.com/thomaspoignant/go-feature-flag/exporter/fileexporter"
	"github.com/thomaspoignant/go-feature-flag/ffcontext"
	"github.com/thomaspoignant/go-feature-flag/retriever/fileretriever"
)

func main() {
	err := ffclient.Init(ffclient.Config{
		PollingInterval: 10 * time.Second,
		Logger:          log.New(os.Stdout, "", 0),
		Context:         context.Background(),
		Retriever: &fileretriever.Retriever{
			Path: "config/flags.gitlab.yaml",
		},
		DataExporter: ffclient.DataExporter{
			FlushInterval:    1 * time.Second,
			MaxEventInMemory: 100,
			Exporter: &fileexporter.Exporter{
				Format:    "json",
				OutputDir: "./evaluation-files/",
				Filename:  " enable_metrics-flag-{{ .Timestamp}}.{{ .Format}}",
			},
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer ffclient.Close()

	user := ffcontext.NewEvaluationContext(uuid.NewString())
	userCanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", user, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if !userCanEnableMetrics {
		fmt.Println("user without admin access cannot enable metrics")
	}

	anonymousUser := ffcontext.NewEvaluationContextBuilder(uuid.NewString()).
		AddCustom("anonymous", true).
		Build()
	anonymUserCanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", anonymousUser, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if !anonymUserCanEnableMetrics {
		fmt.Println("anonymous user without admin access cannot enable metrics")
	}

	userUUID := uuid.NewString()
	adminUser := ffcontext.NewEvaluationContextBuilder(userUUID).
		AddCustom("firstname", "Grace").
		AddCustom("email", "grace@gmail.com").
		AddCustom("admin", true).
		Build()
	CanEnableMetrics, err := ffclient.BoolVariation("enable_metrics", adminUser, false)
	if err != nil {
		log.Printf("could not get flag: %v", err)
	}
	if CanEnableMetrics {
		fmt.Println("Admin user (Grace) can enable metrics")
	}
	userCtx := ffcontext.NewEvaluationContext(userUUID)
	eval, err := ffclient.AllFlagsState(userCtx).MarshalJSON()
	if err != nil {
		log.Printf("could not marshall flag states into JSON for user %s: %v", userUUID, err)
	}
	fmt.Println("user flag evaluation: ")
	var evalRes any
	err = json.Unmarshal(eval, &evalRes)
	if err != nil {
		log.Printf("could not unmarshall flag states: %v", err)
	}
	fmt.Println(evalRes)
	fmt.Println(time.Now().Add(time.Hour * 4).Format(time.RFC3339))
}
