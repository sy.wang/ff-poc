package main

import (
	"context"
	"gitlab.com/sy.wang/ff-poc/flipt/pkg/server"
	sdk "go.flipt.io/flipt/sdk/go"
	flipthttp "go.flipt.io/flipt/sdk/go/http"
	"log/slog"
	"net/http"
)

func main() {
	t := flipthttp.NewTransport("http://localhost:8080")
	s := server.NewServer(sdk.New(t))

	http.HandleFunc("/metrics", HandlerWithUserContext(s.Metrics))
	slog.Info("Listening", "port", "8000")
	http.ListenAndServe(":8000", nil)
}

func HandlerWithUserContext(handler http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user := r.URL.Query().Get("user")
		if user == "" {
			user = "default"
		}

		org := r.URL.Query().Get("org")
		if org == "" {
			org = "default"
		}

		ctx := context.WithValue(r.Context(), "user", user)
		ctx = context.WithValue(ctx, "org", org)
		handler(w, r.WithContext(ctx))
	}
}
