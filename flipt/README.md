## Example

A feature flag named `enable-metrics` is created with the configuration below.
```
version: "1.2"
namespace: default
flags:
  - key: enable-metrics
    name: Enable Metrics
    type: BOOLEAN_FLAG_TYPE
    enabled: false
```
### Use Metrics With Default Feature Flag Setting
An API endpoint `/metrics` was created, which returns either `"no metrics"` or `"cpu.usage: 100%"`. By default,
the `enable-metrics` feature flag is disabled. When we send a curl request to this endpoint, we should see that
metrics is disabled.

```
curl -w "\nTotal: %{time_total}s\n" "http://localhost:8000/metrics"
```

### Result
```
"no metrics"

Total: 0.006122s
```

### Use Metrics With Segment
Now add a segment (user group) for internal developers and add a rollout target to enable metrics for internal
developers.
```
version: "1.2"
namespace: default
flags:
  - key: enable-metrics
    name: Enable Metrics
    type: BOOLEAN_FLAG_TYPE
    enabled: false
    rollouts:
      - segment:
          key: internal-developers
          value: true
segments:
  - key: internal-developers
    name: Internal Developers
    match_type: ANY_MATCH_TYPE
    constraints:
      - property: org
        operator: eq
        value: internal
        type: STRING_COMPARISON_TYPE
```

While sending a curl request to the `/metrics` endpoint, specify the `org` parameter is `internal` only.
```
curl -w "\nTotal: %{time_total}s\n" "http://localhost:8000/metrics?org=internal"
```

### Result
```
"cpu.usage: 100%"

Total: 0.022731s
```
