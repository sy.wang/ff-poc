package server

import (
	"context"
	"encoding/json"
	"go.flipt.io/flipt/rpc/flipt/evaluation"
	sdk "go.flipt.io/flipt/sdk/go"
	"net/http"
)

type Server struct {
	flipt sdk.SDK
}

func NewServer(flipt sdk.SDK) *Server {
	return &Server{flipt: flipt}
}

func (s *Server) Metrics(w http.ResponseWriter, r *http.Request) {
	flag, _ := s.flipt.Evaluation().Boolean(r.Context(), &evaluation.EvaluationRequest{
		FlagKey:  "enable-metrics",
		EntityId: getUser(r.Context()),
		Context: map[string]string{
			"org": getOrganization(r.Context()),
		},
	})
	m := "no metrics"
	if flag.Enabled {
		m = "cpu.usage: 100%"

	}
	if err := json.NewEncoder(w).Encode(m); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func getUser(ctx context.Context) string {
	return ctx.Value("user").(string)
}

func getOrganization(ctx context.Context) string {
	return ctx.Value("org").(string)
}
